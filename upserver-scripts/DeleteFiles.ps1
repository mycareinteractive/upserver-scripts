Param(
[switch] $help,
[string] $path,
[string] $fileType,
[int] $olderThanDays
)

function Usage
{
	Write-Host "Eg: DeleteFiles -path:C:\logs -fileType:*.log -olderThanDays:30"
    Write-Host "The script accepts the below named params"
    Write-Host "-path ->  where log files are located"
    Write-Host "-fileType -> what file/files to delete, like log.txt or *.log"
    Write-Host "-olderThanDays -> if specified only delete files older than X days"
    Write-Host "-help  -> display usage "
}

if([string]::IsNullOrEmpty($path)) 
{
    Usage
}
elseif($help)
{
    Usage
}
else
{
    if($olderThanDays -eq $null)
	{
        $Files = Get-ChildItem $path -Include $fileType -Recurse -Attributes !Directory+!System
    }
    else
	{ 
        $Files = Get-ChildItem $path -Include $fileType -Recurse -Attributes !Directory+!System | WHERE{($_.CreationTime -le $(Get-Date).AddDays(-$olderThanDays))}
    }

    Write-Host "" 
    Write-Host "--------------------------------------------------------------------------"

    foreach ($File in $Files)
    {
        if ($File -ne $Null)
        {
            Write-Host "Deleting file > "$File.Fullname
            Remove-Item -Recurse -Confirm:$false $File.Fullname | out-null      
        }
    }
}